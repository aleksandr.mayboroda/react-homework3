import React from "react"

import FavoritesTable from "../../components/FavoritesTable"

const PageFavorites = ({ productsInFavorite, manageFavorites }) => {
    return (
        <FavoritesTable
            productsInFavorite={productsInFavorite}
            manageFavorites={manageFavorites}
        />
    )
}

export default PageFavorites
