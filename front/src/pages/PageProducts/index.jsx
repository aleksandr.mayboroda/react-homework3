import React from "react"
import "./style.scss"

import ProductCard from "../../components/ProductCard"
import EmptyEntity from "../../components/EmptyEntity"

function PageProducts({
    productList,
    manageCart,
    ckeckInCart,
    manageFavorites,
    checkInFavorites,
}) {
    return (
        <div className="product-page">
            {productList.length <= 0 && (
                <EmptyEntity ><p>there is no products... yeat... i hope...</p></EmptyEntity>
            )}
            {productList.length > 0 && (
                <div className="product-page__list">
                    {productList.map((product) => (
                        <ProductCard
                            key={product.articul}
                            product={product}
                            cartHandler={manageCart}
                            isInCart={ckeckInCart(product.articul)}
                            favoriteHandler={manageFavorites}
                            isInFavorites={checkInFavorites(product.articul)}
                        />
                    ))}
                </div>
            )}
        </div>
    )
}

export default PageProducts
