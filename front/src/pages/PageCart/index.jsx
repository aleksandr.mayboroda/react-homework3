import React from 'react'

import ProductsTable from '../../components/ProductsTable'

function PageCart({productsOnCart, manageCart}) {
    return (
        <ProductsTable productsOnCart={productsOnCart} manageCart={manageCart} />
    )
}

export default PageCart
