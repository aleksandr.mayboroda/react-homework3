import React from "react"
import { Redirect, Route, Switch } from "react-router-dom"

import PageProducts from "../pages/PageProducts"
import PageCart from "../pages/PageCart"
import PageFavorites from "../pages/PageFavorites"

import Page404 from "../pages/Page404"

const AppRoute = ({
    productList,
    manageCart,
    ckeckInCart,
    manageFavorites,
    checkInFavorites,
    productsOnCart,
    productsInFavorite,
}) => {
    return (
        <div>
            <Switch>
                {/* моя идея, пока что */}
                <Redirect exact from="/" to="/products" />

                <Route exact path="/">
                    Главная страница
                </Route>
                <Route exact path="/products">
                    <PageProducts
                        productList={productList}
                        manageCart={manageCart}
                        ckeckInCart={ckeckInCart}
                        manageFavorites={manageFavorites}
                        checkInFavorites={checkInFavorites}
                    />
                </Route>
                <Route exact path="/cart">
                    <PageCart
                        productsOnCart={productsOnCart}
                        manageCart={manageCart}
                    />
                </Route>
                <Route exact path="/favorites">
                    <PageFavorites
                        productsInFavorite={productsInFavorite}
                        manageFavorites={manageFavorites}
                        checkInFavorites={checkInFavorites}
                    />
                </Route>
                <Route path="*">
                    <Page404 />
                </Route>
            </Switch>
        </div>
    )
}

export default AppRoute
