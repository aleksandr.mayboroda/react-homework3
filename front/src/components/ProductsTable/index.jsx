import React, { useEffect, useState } from "react"
import "./style.scss"

import PropTypes from "prop-types"

import axios from "axios"

import EmptyEntity from "../EmptyEntity"
import Button from "../Button"
import ProductTableItem from "../ProductTableItem"

function ProductsTable({ productsOnCart, manageCart }) {
    const [productsData, setProductsData] = useState([])

    const [isOpenedModal, setIsOpenedModal] = useState(false)
    const modalToggle = () => {
        setIsOpenedModal(!isOpenedModal)
    }

    useEffect(() => {
        let all = productsOnCart.map((prod) =>
            axios(`/api/products/${prod.articul}`)
        )
        Promise.all(all).then((resps) => {
            const allPropds = []
            resps.forEach((resp) => {
                if (resp.status === 200) {
                    const isFind = productsOnCart.find(
                        (prod) => prod.articul === resp.data.articul
                    )
                    allPropds.push({ ...resp.data, quantity: isFind.quantity })
                }
            })
            setProductsData(allPropds)
        })
    }, [productsOnCart])

    return (
        <div className="products-table">
            {!productsData.length && (
                <EmptyEntity>
                    <p>your cart is empty</p>
                </EmptyEntity>
            )}
            {productsData.length > 0 && (
                <>
                    <table className="products-table__table">
                        <thead>
                            <tr>
                                <th>image</th>
                                <th>product</th>
                                <th>price</th>
                                <th>quantity</th>
                                <th>sum</th>
                                <th>delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {productsData.map((product) => (
                                <ProductTableItem
                                    key={product.articul}
                                    product={product}
                                    isOpenedModal={isOpenedModal}
                                    modalToggle={modalToggle}
                                    manageCart={manageCart}
                                />
                            ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan="3"></td>
                                <td>To Pay:</td>
                                <td>
                                    {productsData.reduce((a, b) => {
                                        if (a instanceof Object) {
                                            return +a.price + +b.price
                                        } else {
                                            return +a + +b.price
                                        }
                                    }, 0)}
                                    $
                                </td>
                                <td>
                                    <Button
                                        text="Make Order"
                                        func={() => alert('It\'ll be soon ... Maybee...')}
                                        className="btn-success"
                                    />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div className="order"></div>
                </>
            )}
        </div>
    )
}

ProductsTable.propTypes = {
    productsOnCart: PropTypes.arrayOf(
        PropTypes.shape({
            articul: PropTypes.string.isRequired,
            price: PropTypes.number,
            quantity: PropTypes.number.isRequired,
        })
    ),
    manageCart: PropTypes.func.isRequired,
}

export default ProductsTable
