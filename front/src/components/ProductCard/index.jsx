import React, { useState } from "react";
import "./style.scss";

import PropTypes from 'prop-types'
import Button from "../Button";
import Favorite from "../Favorite";
import Modal from "../Modal";

const ProductCard = ({product, cartHandler, isInCart, favoriteHandler, isInFavorites}) => 
{
  const [isOpenedModal, setIsOpenedModal] = useState(false)
  const { name, price = 0, imagePath, articul, color } = product;

  const modalToggle = () =>
  {
    setIsOpenedModal(!isOpenedModal)
  }
  return (
    // onMouseEnter={() => console.log('hovered')}
    <div className="product">
      <div className="product__image">
        <img src={imagePath} alt={name} />
      </div>
      <div className="product__info">
        <h3 className="product__name">{name}</h3>

        <p className="product__articul">articul: {articul}</p>
        <p className="product__price">{price} $</p>
        <p className="product__color">color: <span style={{color}}>{color}</span></p>
        <div className="product__actions">
          <Favorite
            func={favoriteHandler}
            art={articul}
            filled={isInFavorites}
          />
          <Button
            text={isInCart ? "In cart" : "Add to cart"}
            className={isInCart ? "btn-yellow" : "btn-water"}
            func={modalToggle}
          />
          {isOpenedModal && (
            <Modal
              header={"Warning"}
              text={isInCart ? <p>Are you shure you want to remove "{name}" from cart?</p> : <p>Are you shure you want to add "{name}" to cart?</p>}
              handler={modalToggle}
              actions={
                <>
                  <Button
                    backgroundColor={"#b3382c"}
                    func={() => {
                      cartHandler({articul,price,quantity: 1})
                      modalToggle()
                    }}
                    text={isInCart ? "Remove" : "Add"}
                  />
                  <Button
                    backgroundColor={"#b3382c"}
                    func={modalToggle}
                    text={"Cancel"}
                  />
                </>
              }
            />
          )}
        </div>
      </div>
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.exact({
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    imagePath: PropTypes.string,
    articul: PropTypes.string,
    color: PropTypes.string,
  }),
  cartHandler: PropTypes.func.isRequired,
  isInCart: PropTypes.bool,
  favoriteHandler: PropTypes.func.isRequired,
  isInFavorites: PropTypes.bool,
}

export default ProductCard;