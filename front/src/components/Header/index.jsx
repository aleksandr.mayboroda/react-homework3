import React from "react"

import "./style.scss"

import CartCounter from "../CartCounter"
import FavoriteCounter from "../FavoriteCounter"
import Sidebar from "../Sidebar"

const Header = ({ productsInFavorite, productsOnCart }) => {
    return (
        <div className="header">
            <div className="header__block">
                <Sidebar />
            </div>
            <div className="header__block">
                <CartCounter
                    counter={productsOnCart.length}
                    sum={
                        productsOnCart.length > 0
                            ? productsOnCart.reduce(
                                  (acc, cur) => acc + cur.price,
                                  0
                              )
                            : 0
                    }
                />
                <FavoriteCounter
                    counter={
                        productsInFavorite.length > 0
                            ? productsInFavorite.length
                            : 0
                    }
                />
            </div>
        </div>
    )
}

export default Header
