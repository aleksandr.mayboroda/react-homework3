import "./Modal.scss";
import PropTypes from "prop-types";

const Modal = ({ header, text, actions, closeButton, handler }) => 
{
  return (
    <div className="modal" onClick={handler}>
      <div className="modal_window" onClick={ev => ev.stopPropagation()}>
        {closeButton && (
          <span className="modal_close" onClick={handler}></span>
        )}
        <div className="modal_header">{header}</div>
        <div className="modal_content">{text}</div>
        {actions && <div className="modal_footer">{actions}</div>}
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.object.isRequired,
  actions: PropTypes.object,
  closeButton: PropTypes.bool,
  handler: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  actions: {},
  closeButton: false,
}

export default Modal;
