import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import axios from "axios"

import "./style.scss"

import EmptyEntity from "../../components/EmptyEntity"
import FavoritesTableItem from "../FavoritesTableItem"

const FavoritesTable = ({ productsInFavorite, manageFavorites }) => 
{
    const [favoritesData, setFavoritesData] = useState([])

    useEffect(() => {
        let all = productsInFavorite.map((prodId) =>
            axios(`/api/products/${prodId}`)
        )
        Promise.all(all).then((resps) => {
            let favorites = []
            resps.forEach((resp) => {
                if (resp.status === 200) {
                    favorites.push(resp.data)
                }
            })
            setFavoritesData(favorites)
        })
    }, [productsInFavorite])

    if (!productsInFavorite.length) {
        return (
            <EmptyEntity>
                <p>your favorites is empty</p>
            </EmptyEntity>
        )
    }

    return (
        <div className="favorites-table">
            <table className="favorites-table__table">
                <thead>
                    <tr>
                        <th>delete</th>
                        <th>image</th>
                        <th>product</th>
                        <th>price</th>
                    </tr>
                </thead>
                <tbody>
                    {favoritesData.map((product) => (
                        <FavoritesTableItem
                            key={product.articul}
                            product={product}
                            manageFavorites={manageFavorites}
                        />
                    ))}
                </tbody>
            </table>
        </div>
    )
}

FavoritesTable.propTypes = {
    productsInFavorite: PropTypes.array.isRequired,
    manageFavorites: PropTypes.func.isRequired,
}

export default FavoritesTable
