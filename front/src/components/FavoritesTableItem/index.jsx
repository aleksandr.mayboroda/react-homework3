import React from "react"
import PropTypes from "prop-types"
import Icon from "../Icon"

const FavoritesTableItem = ({ product, manageFavorites }) => {
    const { articul, name, imagePath, price } = product
    return (
        <tr>
            <td>
                <button
                    className="favorite-delete-btn"
                    onClick={() => manageFavorites(articul)}
                >
                    <Icon type="star" filled size={"medium"} />
                </button>
            </td>
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
        </tr>
    )
}

FavoritesTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
    }).isRequired,
    manageFavorites: PropTypes.func.isRequired,
}

export default FavoritesTableItem
