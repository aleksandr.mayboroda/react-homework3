import React from "react"
import PropTypes from "prop-types"

import Button from "../Button"
import Modal from "../Modal"

function ProductTableItem({ product, isOpenedModal, modalToggle, manageCart }) {
    const { articul, name, imagePath, price, quantity } = product
    return (
        <tr >
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
            <td>{quantity}</td>
            <td>{price * quantity}$</td>
            <td>
                <button
                    className="product-delete-btn"
                    title="delete from cart"
                    onClick={() => {
                        modalToggle()
                    }}
                >
                    x
                </button>

                {isOpenedModal && (
                    <Modal
                        header={"Warning"}
                        text={
                            <p>
                                Are you shure you want to remove product from
                                cart?
                            </p>
                        }
                        handler={modalToggle}
                        actions={
                            <div
                                style={{
                                    textAlign: "center",
                                }}
                            >
                                <Button
                                    backgroundColor={"#b3382c"}
                                    func={() => {
                                        manageCart({
                                            articul,
                                            price,
                                            quantity: 1,
                                        })
                                        modalToggle()
                                    }}
                                    text={"Remove"}
                                />
                                <Button
                                    backgroundColor={"#b3382c"}
                                    func={modalToggle}
                                    text={"Cancel"}
                                />
                            </div>
                        }
                    />
                )}
            </td>
        </tr>
    )
}

ProductTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
    isOpenedModal: PropTypes.bool,
    modalToggle: PropTypes.func.isRequired,
    manageCart: PropTypes.func.isRequired,
}

export default ProductTableItem
