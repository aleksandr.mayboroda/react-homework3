import "./App.css"
// import React, { useState, useEffect } from "react";
import React, { useState, useEffect } from "react"
import axios from "axios"

import AppRoute from "./routes/AppRoute"

import Header from "./components/Header"

const App = () => {
    const [productList, setProductList] = useState([])
    const [productsOnCart, setProductsOnCart] = useState(
        JSON.parse(localStorage.getItem("cart")) || []
    ) //new [] of {}
    const [productsInFavorite, setProductsInFavorite] = useState(
        JSON.parse(localStorage.getItem("favorites")) || []
    ) //new [] of {}

    //functions
    const checkInFavorites = (articul) => {
        return !!productsInFavorite.find((art) => art === articul)
    }

    const manageFavorites = (articul) => {
        if (checkInFavorites(articul)) {
            const favorites = productsInFavorite.filter(
                (art) => art !== articul
            )
            setProductsInFavorite(favorites)
            localStorage.setItem("favorites", JSON.stringify(favorites))
        } else {
            const favorites = [...productsInFavorite, articul]
            setProductsInFavorite(favorites)
            localStorage.setItem("favorites", JSON.stringify(favorites))
        }
    }

    const ckeckInCart = (articul) => {
        return !!productsOnCart.find((product) => product.articul === articul)
    }

    const manageCart = (productInfo) => {
        if (ckeckInCart(productInfo.articul)) {
            const products = productsOnCart.filter(
                (product) => product.articul !== productInfo.articul
            )
            setProductsOnCart([...products])
            localStorage.setItem("cart", JSON.stringify([...products]))
        } else {
            const products = [...productsOnCart, productInfo]
            setProductsOnCart(products)
            localStorage.setItem("cart", JSON.stringify(products))
        }
    }
    //functions

    useEffect(() => {
        axios("/api/products") //запрос на мой сервер
            .then((response) => {
                setProductList(response.data)
            })
            .catch((err) => console.log("error", err))
    }, [])

    return (
        <div className="container">
            <Header
                productsOnCart={productsOnCart}
                productsInFavorite={productsInFavorite}
            />
            <AppRoute
                productList={productList}
                manageCart={manageCart}
                ckeckInCart={ckeckInCart}
                manageFavorites={manageFavorites}
                checkInFavorites={checkInFavorites}
                productsOnCart={productsOnCart}
                productsInFavorite={productsInFavorite}
            />
        </div>
    )
}

export default App
